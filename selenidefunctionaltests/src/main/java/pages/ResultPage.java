package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class ResultPage {

    public SelenideElement resultMessage(String result){
        return $(byText(result));
    }

}
