package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class RecruitmentFormPage {
    @FindBy(how = How.ID, using = "formNameInput")
    private SelenideElement fullNameInput;

    @FindBy(how = How.ID, using = "formEmailInput")
    private SelenideElement emailInput;

    @FindBy(how = How.XPATH, using = "//label[contains(text(),'Experience in years*')]/following-sibling::*/input")
    private SelenideElement exeperienceInYearsInput;

    @FindBy(how = How.ID, using = "formRoleSelect")
    private SelenideElement roleSelect;

    @FindBy(how = How.ID, using = "formAboutYouTextArea")
    private SelenideElement aboutYouInput;

    @FindBy(how = How.ID, using = "formTermsAndConditionsCheckbox")
    private SelenideElement termsAndConditionsCheckbox;

    @FindBy(how = How.XPATH, using = "//a//following-sibling::*[@class='form-group']")
    private SelenideElement clearedTermsAndConditionsCheckbox;

    @FindBy(how = How.ID, using = "formSubmitButton")
    private SelenideElement submitButton;

    @FindBy(how = How.ID, using = "formClearButton")
    private SelenideElement clearButton;

    @FindBy(how = How.CLASS_NAME, using = "help-block")
    private ElementsCollection validationErrorMessages;

    public void typeFullName(String fullName) {
        fullNameInput.setValue(fullName);
    }

    public void typeEmail(String email) {
        emailInput.setValue(email);
    }

    public void typeExperienceInYears(String years) {
        exeperienceInYearsInput.setValue(years);
    }

    public void selectRole(String role) {
        roleSelect.click();
        $(byText(role)).click();
    }

    public void typeAboutYou(String aboutYou) {
        aboutYouInput.setValue(aboutYou);
    }

    public void confirmTermsAndConditions() {
        termsAndConditionsCheckbox.click();
    }

    public ResultPage submit() {
        submitButton.click();
        return Selenide.page(ResultPage.class);
    }

    public void clear() {
        clearButton.click();
    }

    public SelenideElement recruitmentForm(){
        return $(byXpath("//form"));
    }

    public SelenideElement validationErrorMessage(String message){
        return validationErrorMessages.findBy(text(message));
    }

    public boolean isCleared() {
        if (!fullNameInput.is(empty)) {
            System.out.println("Field Full Name is not cleared.");
            return false;
        }
        if (!emailInput.is(empty)) {
            System.out.println("Field Email is not cleared.");
            return false;
        }
        if (!exeperienceInYearsInput.is(empty)) {
            System.out.println("Field Experience in years is not cleared.");
            return false;
        }
        if (!roleSelect.is(text("Select Role"))) {
            System.out.println("Field Role is not cleared.");
            return false;
        }
        if (!aboutYouInput.is(empty)) {
            System.out.println("Field About you is not cleared.");
            return false;
        }
        if (!clearedTermsAndConditionsCheckbox.exists()) {
            System.out.println("Terms an Conditions checkbox is not cleared.");
            return false;
        }
        return true;
    }
}
