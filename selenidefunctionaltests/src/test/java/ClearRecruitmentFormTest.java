import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.RecruitmentFormPage;

public class ClearRecruitmentFormTest extends FillRecruitmentFormTest {

    @BeforeClass
    public void setUp(){
        Configuration.browser = "chrome";
        Configuration.baseUrl = "http://qa-recruitment-form.s3-website-eu-west-1.amazonaws.com";
        form = Selenide.open("/", RecruitmentFormPage.class);
    }

    @Test(dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenRecruitmentFormFilled_whenClearButtonClicked_thenFormClearedSuccessfully(){
        form.typeFullName("Test");
        form.typeEmail("test@test.pl");
        form.typeExperienceInYears("2");
        form.selectRole("Test Automation Engineer");
        form.typeAboutYou("A curiosity about candidate...");
        form.confirmTermsAndConditions();
        form.clear();
        Assert.assertTrue(form.isCleared());
    }
}
