import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.*;
import com.codeborne.selenide.Configuration;
import pages.RecruitmentFormPage;

public class FillRecruitmentFormTest {

    protected RecruitmentFormPage form;

    @BeforeClass
    public void setUpTestClass() {
        Configuration.browser = "chrome";
        Configuration.baseUrl = "http://qa-recruitment-form.s3-website-eu-west-1.amazonaws.com";
    }

    @BeforeMethod
    public void setUpTestMethod() {
        form = Selenide.open("/", RecruitmentFormPage.class);
    }

    @Test
    public void givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly() {
        form.recruitmentForm().should(Condition.exist);
    }

    @DataProvider
    public Object[][] Roles() {
        return new Object[][]{
                {"Frontend Developer"},
                {"Full Stack Developer"},
                {"Android Engineer"},
                {"iOS Engineer"},
                {"Test Automation Engineer"}
        };
    }

    @Test(dataProvider = "Roles", dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenRequiredFieldsFilledCorrectly_whenRecruitmentFormSubmit_thenApplicationSentSuccessfully(String role) {
        form.typeFullName("Test");
        form.typeEmail("test@test.pl");
        form.typeExperienceInYears("2");
        form.selectRole(role);
        form.confirmTermsAndConditions();
        form.submit().resultMessage("Success!").should(Condition.exist);
    }

    @Test(dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenAllFieldsFilledCorrectly_whenRecruitmentFormSubmit_thenApplicationSentSuccessfully() {
        form.typeFullName("Test");
        form.typeEmail("test@test.pl");
        form.typeExperienceInYears("2");
        form.selectRole("Test Automation Engineer");
        form.typeAboutYou("A curiosity about candidate...");
        form.confirmTermsAndConditions();
        form.submit().resultMessage("Success!").should(Condition.exist);
    }

    @Test(dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenExperienceLessThanTwoYears_whenRecruitmentFormSubmit_thenFailurePageDisplayedCorrectly() {
        form.typeFullName("Test");
        form.typeEmail("test@test.pl");
        form.typeExperienceInYears("1");
        form.selectRole("Test Automation Engineer");
        form.confirmTermsAndConditions();
        form.submit().resultMessage("Failed!").should(Condition.exist);
    }
}
