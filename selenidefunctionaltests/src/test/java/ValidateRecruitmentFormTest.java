import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.RecruitmentFormPage;

public class ValidateRecruitmentFormTest extends FillRecruitmentFormTest {

    @BeforeClass
    public void setUpTestClass(){
        Configuration.browser = "chrome";
        Configuration.baseUrl = "http://qa-recruitment-form.s3-website-eu-west-1.amazonaws.com";
    }

    @BeforeMethod
    public void setUpTestMethod() {
        form = Selenide.open("/", RecruitmentFormPage.class);
    }

    @DataProvider
    public Object[][] EmptyFormValidationErrorMessages() {
        return new Object[][]{
                {"The name is too short"},
                {"The email is incorrect"},
                {"Please type your experience"},
                {"Please select your future role"},
                {"Please check to proceed"}
        };
    }

    @Test(dataProvider = "EmptyFormValidationErrorMessages", dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenRequiredFieldsEmpty_whenRecruitmentFormSubmit_thenValidationErrorMessagesDisplayed(String ValidationErrorMessage) {
        form.submit();
        form.validationErrorMessage(ValidationErrorMessage).should(Condition.exist);
    }

    @DataProvider
    public Object[][] InvalidFullNameValidationData() {
        return new Object[][]{
                {"A","The name is too short"},
                {"A123", "The name is incorrect"},
                {"A!@#", "The name is incorrect"}
        };
    }

    @Test(dataProvider = "InvalidFullNameValidationData", dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenInvalidDataTypedInFullNameField_whenRecruitmentFormSubmit_thenApplicationSentSuccessfully(String name, String ValidationErrorMessage) {
        form.typeFullName(name);
        form.validationErrorMessage(ValidationErrorMessage).should(Condition.exist);
    }

    @DataProvider
    public Object[][] InvalidEmailValidationData() {
        return new Object[][]{
                {"email", "The email is incorrect"},
                {"email@email@test.pl", "The email is incorrect"},
                {"e mail@test.pl", "The email is incorrect"},
                {"email@test!.pl", "The email is incorrect"},
                {"email@test,.pl", "The email is incorrect"}
        };
    }

    @Test(dataProvider = "InvalidEmailValidationData", dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenInvalidDataTypedInEmailField_whenRecruitmentFormSubmit_thenApplicationSentSuccessfully(String email, String ValidationErrorMessage) {
        form.typeEmail(email);
        form.validationErrorMessage(ValidationErrorMessage).should(Condition.exist);
    }

    @DataProvider
    public Object[][] InvalidExperienceInYearsValidationData() {
        return new Object[][]{
                {"-5","Your experience looks wired!"},
                {"100", "Your experience looks wired!"}
        };
    }

    @Test(dataProvider = "InvalidExperienceInYearsValidationData", dependsOnMethods={"givenSupportedBrowser_whenRecruitmentFormOpen_thenFormDisplayedCorrectly"})
    public void givenInvalidDataTypedInExperienceInYears_whenRecruitmentFormSubmit_thenApplicationSentSuccessfully(String years, String ValidationErrorMessage) {
        form.typeExperienceInYears(years);
        form.validationErrorMessage(ValidationErrorMessage).should(Condition.exist);
    }
}
